<?php

/* SmartBookLecteurBundle:lecteur:index.html.twig */
class __TwigTemplate_ebe6ca9cf1b7cf99915f8a5c1334b9abc6f249ae4ae87640f2c39e7a3d4cf0f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        try {
            $this->parent = $this->env->loadTemplate("SmartBookLecteurBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(2);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    
    <h1>Ici view de lecteur </h1>  
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:lecteur:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 4,  36 => 3,  11 => 2,);
    }
}
